﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownInfo : MonoBehaviour {

    public Transform marketLocation;
    public Transform marketStealLocation;

    public Transform tavernLocation;
    public Transform tavernDrink1Location;
    public Transform tavernDrink2Location;

    public Transform churchLocation;
    public Transform fishingLocation;
}

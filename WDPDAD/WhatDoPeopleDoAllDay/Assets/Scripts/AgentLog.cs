﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// used for logging agent behaviour

public class AgentLog {

    //public ActionBehaviour action;

    public string action;


    public float startTime = 0.0f;
    public float endTime = 0.0f;

    public AgentLog(string _action, float _startTime)
    {
        action = _action;
        startTime = _startTime;
    }

    public float EndTime
    {
        get
        {
            return endTime;
        }
        set
        {
            endTime = value;
        }
        
    }
}
